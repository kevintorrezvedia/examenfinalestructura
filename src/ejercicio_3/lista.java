/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejercicio_3;

/**
 *
 * @author Kevin Angel Torrez Vedia     8514889
 */
public class lista<T> {
    private nodo<T> primero;
    private nodo<T> ultimo;
    private int tamaño;

    public lista() {
        this.primero = null;
        this.ultimo = null;
        this.tamaño = 0;
    }

    public boolean vacio() {
        return tamaño == 0;
    }

    public int tamañolista() {
        return tamaño;
    }

    private nodo<T> getNode(int index) {
        if (vacio() || (index < 0 || index >= tamañolista())) {
            return null;
        } else if (index == 0) {
            return primero;
        } else if (index == tamañolista() - 1) {
            return ultimo;
        } else {
            nodo<T> search = primero;
            int contador = 0;
            while (contador < index) {
                contador++;
                search = search.getNext();
            }
            return search;
        }
    }

    public T sacar(int index) {
        if (vacio() || (index < 0 || index >= tamañolista())) {
            return null;
        } else if (index == 0) {
            return primero.getElement();
        } else if (index == tamañolista() - 1) {
            return ultimo.getElement();
        } else {
            nodo<T> search = getNode(index);
            return search.getElement();
        }
    }

    public T sacarprimero() {
        if (vacio()) {
            return null;
        } else {
            return primero.getElement();
        }
    }

    public T sacarultimo() {
        if (vacio()) {
            return null;
        } else {
            return ultimo.getElement();
        }
    }
     public T agregarprimero(T element) {
        nodo<T> newelement;
        if (vacio()) {
            newelement = new nodo<>(element, null);
            primero = newelement;
            ultimo = newelement;
        } else {
            newelement = new nodo<T>(element,primero);
            primero = newelement;
        }
        tamaño++;
        return primero.getElement();
    }
     public String contenidodelista(){
        String str = "";
        if(vacio()){
            str = "La lista esta vacia";
        }else{
           nodo<T> out = primero;
           while(out != null){
               str += out.getElement()+" / ";
               out = out.getNext();
           }
        }
        return str;
    }
}
