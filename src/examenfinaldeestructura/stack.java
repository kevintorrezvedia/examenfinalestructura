/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package examenfinaldeestructura;

/**
 *
 * @author Kevin Angel Torrez Vedia     8514889
 */
public class stack {
    char arr[];
    int top;
    
    stack(){
        this.arr = new char[10];
        this.top = -1;
    }
    public boolean isEmpty(){
        return (top == -1);
    }
    public void push(char element){
        top++;
        if(top < arr.length){
            arr[top] = element;
        }else{
            char temp[] = new char[arr.length+5];
            for(int i = 0; i<arr.length; i++){
                temp[i] = arr[i];
            }
            arr = temp;
            arr[top] = element;
        }
    }
    public char peek(){
        return arr[top];
    }
    public char pop(){
        if(!isEmpty()){
            int temptop = top;
            top--;
            char returntop = arr[temptop];
            return returntop;
        }else{
            return '/';
        }
    }
}
